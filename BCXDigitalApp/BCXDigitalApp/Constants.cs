﻿using System;
namespace BCXDigitalApp
{
    public class Constants
    {
        public static string NEWS_SERVICE_REST_URL = "https://bcxnewsfeedapi.azurewebsites.net/api/newsfeed";
        public static string BLOB_STORAGE_REST_URL = "https://highvelocitystorage.blob.core.windows.net/employee-app/";
    }
}
