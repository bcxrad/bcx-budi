﻿using System;
using System.Collections.Generic;

namespace BCXDigitalApp.Models
{
    public class NewsFeeds
    {
        public string transactionId { get; set; }
        public List<NewsEntries> newsEntries { get; set; }

        public class NewsEntries
        {
            public string imageUrl { get; set; }
            public string heading { get; set; }
            public string body { get; set; }
        }
    }


}
