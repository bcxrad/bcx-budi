﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BCXDigitalApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PickEmployeePage : ContentPage
	{
		public PickEmployeePage ()
		{
			InitializeComponent ();
            preArrivalBG.Source = ImageSource.FromResource("BCXDigitalApp.Images.newbg.joel-filipe-287425-unsplash.jpg");
            logoPNG.Source = ImageSource.FromResource("BCXDigitalApp.Images.bg.ambassador-body_11_db1208439cebaf075740967f84e858d6.png");
        }
        private async void ExistingEmployeeBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
        }

        private async void NewEmployeeBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewEmployeePage());
        }



    }
}