﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BCXDigitalApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LandingPage : ContentPage
	{
		public LandingPage ()
		{
			InitializeComponent ();

            logoPic.Source = ImageSource.FromResource("BCXDigitalApp.Images.bg.ambassador-body_11_db1208439cebaf075740967f84e858d6.png");
            image.Source = ImageSource.FromResource("BCXDigitalApp.Images.PicsArt_05-23-02.51.56.jpg");
            
        }
        private async void EventPageBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new EventPage());
        }
        private async void CanteenPageBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CanteenPage());
        }
        private async void MyProfileBtnClick(object sender , EventArgs e)
        {
            await Navigation.PushAsync(new MyProfilePage());
        }
        private async void GymBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new GymPage());
        }
        private async void NewsBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewsPage());
        }
    }
}