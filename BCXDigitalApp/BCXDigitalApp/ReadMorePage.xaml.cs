﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BCXDigitalApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ReadMorePage : ContentPage
	{
		public ReadMorePage ()
		{
            InitializeComponent();
            image.Source = ImageSource.FromResource("BCXDigitalApp.Images.bg.photo-1470790376778-a9fbc86d70e2.jpg");
        }
	}
}