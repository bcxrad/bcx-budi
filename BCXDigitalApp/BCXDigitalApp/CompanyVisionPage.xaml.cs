﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BCXDigitalApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CompanyVisionPage : ContentPage
	{
		public CompanyVisionPage ()
		{
			InitializeComponent ();
            logoPNG.Source = ImageSource.FromResource("BCXDigitalApp.Images.bg.ambassador-body_11_db1208439cebaf075740967f84e858d6.png");
            companyVisionBG.Source = ImageSource.FromResource("BCXDigitalApp.Images.newbg.photo-1470290449668-02dd93d9420a.jpg");
        }
	}
}