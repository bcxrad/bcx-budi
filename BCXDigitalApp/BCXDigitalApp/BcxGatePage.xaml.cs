﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BCXDigitalApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BcxGatePage : ContentPage
	{
		public BcxGatePage ()
		{
			InitializeComponent ();
            image.Source = ImageSource.FromResource("BCXDigitalApp.Images.newbg.anchor-lee-181406-unsplash.jpg");
        }
         private async void LandingPageBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new LandingPage());
        }

	}
}