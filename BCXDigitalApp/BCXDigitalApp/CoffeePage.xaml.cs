﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BCXDigitalApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CoffeePage : ContentPage
	{
		public CoffeePage ()
		{
			InitializeComponent ();
            logoPic.Source = ImageSource.FromResource("BCXDigitalApp.Images.bg.ambassador-body_11_db1208439cebaf075740967f84e858d6.png");
            coffeeBG.Source = ImageSource.FromResource("BCXDigitalApp.Images.newbg.photo-1470290449668-02dd93d9420a.jpg");
            coffeePic.Source = ImageSource.FromResource("BCXDigitalApp.Images.food.Coffee-Cup-PNG-Pic.png");
            coffeePic2.Source = ImageSource.FromResource("BCXDigitalApp.Images.food.Coffee-Cup-PNG-Pic.png");
            coffeePic3.Source = ImageSource.FromResource("BCXDigitalApp.Images.food.Coffee-Cup-PNG-Pic.png");
        }
	}
}