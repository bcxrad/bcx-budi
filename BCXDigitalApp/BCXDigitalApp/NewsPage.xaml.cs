﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCXDigitalApp.Adapters;
using BCXDigitalApp.Models;
using BCXDigitalApp.Services;
using BCXDigitalApp.UI;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BCXDigitalApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewsPage : CarouselPage
    {
        public NewsPage()
        {
            InitializeComponent();
            SyncNewsData();
        }

        private async void SyncNewsData()
        {
            NewsFeed newsFeed;
            IRestFul<Task<NewsFeed>> newsService = new NewsService();
            var loadingPage = ActivityIndicatorPage.LoadIndicator();
            Children.Add(loadingPage);
            newsFeed = await newsService.Get();
            Children.Remove(loadingPage);
            Thickness padding;
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                case Device.Android:
                    padding = new Thickness(10, 0, 10, 0);
                    break;
                default:
                    padding = new Thickness(10, 0, 10, 0);
                    break;
            }
            foreach(NewsEntry newsEntry in newsFeed.newsEntries) 
            {
                
                var contentPage = new ContentPage
                {
                    //Padding = padding,
                    Content = new StackLayout
                    {
                        Children = {
                            new ScrollView()
                {
                    Orientation = ScrollOrientation.Vertical,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                                Content = new StackLayout
                    {
                                    Children = {
                                        new Image
                {
                    Source = Constants.BLOB_STORAGE_REST_URL + newsEntry.imageUrl,
                    Aspect = Aspect.AspectFill
                },
                            new Label
                {
                                            Margin = new Thickness (10, 0, 10, 0),
                    Text = newsEntry.heading,
                    FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                    FontAttributes = FontAttributes.Bold,
                                            TextColor = Color.Black
                },
                            new BoxView() { Color = Color.Black, WidthRequest = 100, HeightRequest = 1 },
                            new Label
                {
                                            Margin = new Thickness (10, 0, 10, 0),
                    Text = newsEntry.body,
                    FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label))
                }
                            }
                                }
                            }
                
                }
                    }
                };
                Children.Add(contentPage);
            }
        }
	}
}



// System.Diagnostics.Debug.WriteLine("");