﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BCXDigitalApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GymPage : ContentPage
	{
		public GymPage ()
		{
			InitializeComponent ();
            logoPic.Source = ImageSource.FromResource("BCXDigitalApp.Images.bg.ambassador-body_11_db1208439cebaf075740967f84e858d6.png");
        }
	}
}