﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BCXDigitalApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            image.Source = ImageSource.FromResource("BCXDigitalApp.Images.bg.photo-1492546643178-96d64f3fd824.jpg");
            logoPNG.Source = ImageSource.FromResource("BCXDigitalApp.Images.bg.ambassador-body_11_db1208439cebaf075740967f84e858d6.png");
        }
        private async void ReadMoreBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ReadMorePage());
        }
        private async void SubmitBtnClick(object sender, EventArgs e)
        {
            if (usernameEntry.Text == "barry" && passwordEntry.Text == "barry")
            {
                await Navigation.PushAsync(new LandingPage());
            }
            else if (usernameEntry.Text == "Lethabo" && passwordEntry.Text == "shorty")
            {
                await Navigation.PushAsync(new BcxGatePage());
            }
            else if (usernameEntry.Text == "Hugh" && passwordEntry.Text == "green")
            {
                await Navigation.PushAsync(new BcxGatePage());
            }
            else if (usernameEntry.Text == "Miles" && passwordEntry.Text == "poppelton")
            {
                await Navigation.PushAsync(new BcxGatePage());
            }
            else if (usernameEntry.Text == "Mark" && passwordEntry.Text == "tool")
            {
                await Navigation.PushAsync(new BcxGatePage());
            } 
            else
            {
                messageLabel.Text = "Login Failed";
                passwordEntry.Text = string.Empty;
            }
        }
        
    }

}
