﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BCXDigitalApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CanteenPage : ContentPage
	{
		public CanteenPage ()
		{
			InitializeComponent();
            canteenBackground.Source = ImageSource.FromResource("BCXDigitalApp.Images.bg.photo-1503455637927-730bce8583c0.jpg");
            logoPic.Source = ImageSource.FromResource("BCXDigitalApp.Images.bg.ambassador-body_11_db1208439cebaf075740967f84e858d6.png");
        }
        private async void TodaysMealsBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new TodaysMealPage());
        }
        private async void CoffeeBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CoffeePage());
        }

    }
}