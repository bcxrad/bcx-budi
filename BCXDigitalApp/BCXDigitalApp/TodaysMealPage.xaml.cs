﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BCXDigitalApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TodaysMealPage : ContentPage
	{
		public TodaysMealPage ()
		{
			InitializeComponent ();

            todaysMealBG.Source = ImageSource.FromResource("BCXDigitalApp.Images.newbg.photo-1470290449668-02dd93d9420a.jpg");
            logoPic.Source = ImageSource.FromResource("BCXDigitalApp.Images.bg.ambassador-body_11_db1208439cebaf075740967f84e858d6.png");
            burgerPic.Source = ImageSource.FromResource("BCXDigitalApp.Images.food.burger_bar.png");
            wrapPic.Source = ImageSource.FromResource("BCXDigitalApp.Images.food.wrap.jpg");
            masalaPic.Source = ImageSource.FromResource("BCXDigitalApp.Images.food.masala.png");
        }
	}
}