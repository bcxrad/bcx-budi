﻿using System;
using Xamarin.Forms;

namespace BCXDigitalApp.UI
{
    public class ActivityIndicatorPage
    {
        public ActivityIndicatorPage()
        {
        }

        public static ContentPage LoadIndicator() {
            return new ContentPage
            {
                Content = new ActivityIndicator()
                {
                    IsVisible = true,
                    IsRunning = true,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Center
                }
            };
        }
    }
}
