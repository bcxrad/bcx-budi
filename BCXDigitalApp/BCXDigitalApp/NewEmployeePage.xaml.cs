﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BCXDigitalApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewEmployeePage : ContentPage
	{
		public NewEmployeePage ()
		{
			InitializeComponent ();
            logoPic.Source = ImageSource.FromResource("BCXDigitalApp.Images.bg.ambassador-body_11_db1208439cebaf075740967f84e858d6.png");
            newEmployeeBG.Source = ImageSource.FromResource("BCXDigitalApp.Images.newbg.joel-filipe-287425-unsplash.jpg");
        }
        private async void CanteenPageBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CanteenPage());
        }
        private async void CompanyVisionBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CompanyVisionPage());
        }
        private async void ReturnToAppBtnClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
        }

        
    }
}