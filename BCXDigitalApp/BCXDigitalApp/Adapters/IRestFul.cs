﻿using System;
namespace BCXDigitalApp.Adapters
{
    public interface IRestFul<T>
    {
        T Get();
        void Post();
        void Put();
        void Delete();
        void Patch();
    }
}
