﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BCXDigitalApp.Adapters;
using BCXDigitalApp.Models;
using Newtonsoft.Json;

namespace BCXDigitalApp.Services
{
    public class NewsService : IRestFul<Task<NewsFeed>>
    {
        HttpClient client;

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public void Patch()
        {
            throw new NotImplementedException();
        }

        public void Post()
        {
            throw new NotImplementedException();
        }

        public void Put()
        {
            throw new NotImplementedException();
        }

        public async Task<NewsFeed> Get()
        {
            NewsFeed newsFeed = new NewsFeed();
            var uri = new Uri(string.Format(Constants.NEWS_SERVICE_REST_URL, string.Empty));
            client = new HttpClient();
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                newsFeed = JsonConvert.DeserializeObject<NewsFeed>(content);
                Console.WriteLine(newsFeed.transactionId);
            }
            return newsFeed;
        }
    }
}
